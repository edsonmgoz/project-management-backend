package et.pm.api.repository;

import et.pm.api.entity.Project;
import et.pm.api.entity.TypeProject;
import et.pm.api.pojo.ProjectPojo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
  @Query("SELECT new et.pm.api.pojo.ProjectPojo(p.name, p.description) FROM projects p WHERE p.typeProject = ?1")
  List<ProjectPojo> findByTypeCustom(TypeProject typeProject);
}