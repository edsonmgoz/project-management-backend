package et.pm.api.repository;

import et.pm.api.entity.TypeProject;
import et.pm.api.pojo.TypePojo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeProjectRepository extends JpaRepository<TypeProject, Integer> {
  @Query("SELECT new et.pm.api.pojo.TypePojo(t.name) FROM TypeProject t WHERE t.id = ?1")
  TypePojo findByIdCustom(Integer id);
}