package et.pm.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDto {
    private String code;
    private String name;
    private String description;
    private Boolean active;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer typeProjectId;
}
