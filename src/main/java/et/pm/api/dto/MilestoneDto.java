package et.pm.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class MilestoneDto {
    private String name;
    private String description;
    private LocalDate dueDate;
    private Boolean isCompleted;
    private Integer projectId;
}
