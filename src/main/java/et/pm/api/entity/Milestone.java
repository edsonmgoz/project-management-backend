package et.pm.api.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="milestones")
public class Milestone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 100, nullable = false)
    private String name;

    @Column(length = 250, nullable = false)
    private String description;


    private LocalDate dueDate;
    private Boolean idCompleted;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
