package et.pm.api.mapper;

import et.pm.api.dto.MilestoneDto;
import et.pm.api.entity.Milestone;
import org.springframework.stereotype.Component;

@Component
public class MilestoneMapper {
    public Milestone fromDto(MilestoneDto dto, Milestone milestoneFound) {
        Milestone milestone = new Milestone();
        if (milestoneFound != null) {
            milestone = milestoneFound;
        }
        milestone.setName(dto.getName());
        milestone.setDescription(dto.getDescription());
        milestone.setDueDate(dto.getDueDate());
        milestone.setIdCompleted(dto.getIsCompleted());
        return milestone;
    }
}
