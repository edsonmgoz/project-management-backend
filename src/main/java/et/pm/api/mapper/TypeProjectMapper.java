package et.pm.api.mapper;

import et.pm.api.dto.TypeProjectDto;
import et.pm.api.entity.TypeProject;
import org.springframework.stereotype.Component;

@Component
public class TypeProjectMapper {

    public TypeProject fromDto(TypeProjectDto dto, TypeProject typeProjectFound) {
        TypeProject typeProject = new TypeProject();
        if (typeProjectFound != null) {
            typeProject = typeProjectFound;
        }
        typeProject.setName(dto.getName());
        typeProject.setDescription(dto.getDescription());
        return typeProject;
    }
}
