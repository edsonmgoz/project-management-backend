package et.pm.api.mapper;

import et.pm.api.dto.TaskDto;
import et.pm.api.entity.Task;
import org.springframework.stereotype.Component;

@Component
public class TaskMapper {
    public Task fromDto(TaskDto dto, Task taskFound) {
        Task task = new Task();
        if (taskFound != null) {
            task = taskFound;
        }
        task.setName(dto.getName());
        task.setDescription(dto.getDescription());
        task.setDueDate(dto.getDueDate());
        task.setIdCompleted(dto.getIsCompleted());
        return task;
    }
}
