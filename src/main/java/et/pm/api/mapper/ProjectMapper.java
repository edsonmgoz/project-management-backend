package et.pm.api.mapper;

import et.pm.api.dto.ProjectDto;
import et.pm.api.entity.Project;
import org.springframework.stereotype.Component;

@Component
public class ProjectMapper {
    public Project fromDto(ProjectDto dto, Project projectFound) {
        Project project = new Project();
        if (projectFound != null) {
            project = projectFound;
        }
        project.setCode(dto.getCode());
        project.setName(dto.getName());
        project.setDescription(dto.getDescription());
        project.setActive(dto.getActive());
        project.setStartDate(dto.getStartDate());
        project.setEndDate(dto.getEndDate());
        return project;
    }
}
