package et.pm.api.controller;

import et.pm.api.dto.MilestoneDto;
import et.pm.api.entity.Milestone;
import et.pm.api.service.MilestoneService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/milestones")
public class MilestoneController {

    private MilestoneService milestoneService;

    @GetMapping
    public ResponseEntity<List<Milestone>> getAll() {
        List<Milestone> projects = milestoneService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(projects);
    }

    @PostMapping
    public ResponseEntity<Milestone> create(@RequestBody MilestoneDto dto) {
        Milestone milestoneSaved = milestoneService.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(milestoneSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Milestone> update(@PathVariable Integer id, @RequestBody MilestoneDto dto) {
        Milestone milestoneUpdated = milestoneService.update(id, dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(milestoneUpdated);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Milestone> getById(@PathVariable Integer id) {
        Milestone milestoneFound = milestoneService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(milestoneFound);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id) {
        milestoneService.delete(id);
        return ResponseEntity.ok().build();
    }
}
