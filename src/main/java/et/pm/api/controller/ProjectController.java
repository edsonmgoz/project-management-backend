package et.pm.api.controller;

import et.pm.api.dto.ProjectDto;
import et.pm.api.entity.Project;
import et.pm.api.service.ProjectService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/projects")
public class ProjectController {

    private ProjectService projectService;

    @GetMapping
    public ResponseEntity<List<Project>> getAll() {
        List<Project> projects = projectService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(projects);
    }

    @PostMapping
    public ResponseEntity<Project> create(@RequestBody ProjectDto dto) {
        Project projectSaved = projectService.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(projectSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Project> update(@PathVariable Integer id, @RequestBody ProjectDto dto) {
        Project projectUpdated = projectService.update(id, dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(projectUpdated);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Project> getById(@PathVariable Integer id) {
        Project projectFound = projectService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(projectFound);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id) {
        projectService.delete(id);
        return ResponseEntity.ok().build();
    }
}
