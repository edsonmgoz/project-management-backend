package et.pm.api.controller;

import et.pm.api.dto.TypeProjectDto;
import et.pm.api.entity.TypeProject;
import et.pm.api.pojo.TypeProjectPojo;
import et.pm.api.service.TypeProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.AllArgsConstructor;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/typesProjects")
public class TypeProjectController {

    private TypeProjectService typeProjectService;

    @GetMapping
    public ResponseEntity<List<TypeProject>> getAll() {
        List<TypeProject> typesProjects = typeProjectService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(typesProjects);
    }

    @PostMapping
    public ResponseEntity<TypeProject> create(@RequestBody TypeProjectDto dto) {
        TypeProject typeProjectSaved = typeProjectService.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(typeProjectSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TypeProject> update(@PathVariable Integer id, @RequestBody TypeProjectDto dto) {
        TypeProject typeProjectUpdated = typeProjectService.update(id, dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(typeProjectUpdated);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TypeProject> getById(@PathVariable Integer id) {
        TypeProject typeProjectFound = typeProjectService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(typeProjectFound);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id) {
        typeProjectService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/withProjects/{id}")
    public ResponseEntity<TypeProjectPojo> getByIdWithProjects(@PathVariable Integer id) {
        TypeProjectPojo typeWithProjectsFound = typeProjectService.getByIdWithProjects(id);
        return ResponseEntity.status(HttpStatus.OK).body(typeWithProjectsFound);
    }
}
