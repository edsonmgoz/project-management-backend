package et.pm.api.controller;

import et.pm.api.dto.TaskDto;
import et.pm.api.entity.Task;
import et.pm.api.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/tasks")
public class TaskController {

    private TaskService taskService;

    @GetMapping
    public ResponseEntity<List<Task>> getAll() {
        List<Task> tasks = taskService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(tasks);
    }

    @PostMapping
    public ResponseEntity<Task> create(@RequestBody TaskDto dto) {
        Task taskSaved = taskService.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(taskSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Task> update(@PathVariable Integer id, @RequestBody TaskDto dto) {
        Task taskUpdated = taskService.update(id, dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(taskUpdated);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> getById(@PathVariable Integer id) {
        Task taskFound = taskService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(taskFound);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id) {
        taskService.delete(id);
        return ResponseEntity.ok().build();
    }
}
