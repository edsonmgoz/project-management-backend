package et.pm.api.service;

import et.pm.api.dto.MilestoneDto;
import et.pm.api.entity.Milestone;
import et.pm.api.entity.Project;
import et.pm.api.exception.EntityNotFoundException;
import et.pm.api.mapper.MilestoneMapper;
import et.pm.api.repository.MilestoneRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MilestoneServiceImpl implements MilestoneService {

    private MilestoneRepository milestoneRepository;
    private MilestoneMapper milestoneMapper;
    private ProjectService projectService;

    @Override
    public List<Milestone> getAll() {
        return milestoneRepository.findAll();
    }

    @Override
    public Milestone create(MilestoneDto dto) {
        Milestone milestone = milestoneMapper.fromDto(dto, null);
        Project project = projectService.getById(dto.getProjectId());
        milestone.setProject(project);
        return milestoneRepository.save(milestone);
    }

    @Override
    public Milestone update(Integer id, MilestoneDto dto) {
        Milestone milestoneFound = milestoneRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Milestone", id));
        Project project = projectService.getById(dto.getProjectId());
        Milestone milestone = milestoneMapper.fromDto(dto, milestoneFound);
        milestone.setProject(project);
        return milestoneRepository.save(milestone);
    }

    @Override
    public Milestone getById(Integer id) {
        Milestone milestone = milestoneRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Milestone", id));
        return milestone;
    }

    @Override
    public void delete(Integer id) {
        Milestone milestone = milestoneRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Milestone", id));
        milestoneRepository.delete(milestone);
    }
}
