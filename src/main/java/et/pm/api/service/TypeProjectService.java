package et.pm.api.service;

import et.pm.api.dto.TypeProjectDto;
import et.pm.api.entity.TypeProject;

import et.pm.api.pojo.TypeProjectPojo;
import java.util.List;

public interface TypeProjectService {

    List<TypeProject> getAll();

    TypeProject create(TypeProjectDto typeProject);

    TypeProject update(Integer id, TypeProjectDto typeProject);

    TypeProject getById(Integer id);

    void delete(Integer id);

    TypeProjectPojo getByIdWithProjects(Integer id);
}
