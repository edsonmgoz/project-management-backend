package et.pm.api.service;

import et.pm.api.dto.TypeProjectDto;
import et.pm.api.entity.TypeProject;
import et.pm.api.exception.EntityNotFoundException;
import et.pm.api.pojo.ProjectPojo;
import et.pm.api.pojo.TypePojo;
import et.pm.api.pojo.TypeProjectPojo;
import et.pm.api.repository.TypeProjectRepository;
import et.pm.api.mapper.TypeProjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class TypeProjectServiceImpl implements TypeProjectService {

    private TypeProjectRepository typeProjectRepository;
    private TypeProjectMapper typeProjectMapper;
    private ProjectService projectService;

    @Override
    public List<TypeProject> getAll() {
        return typeProjectRepository.findAll();
    }

    @Override
    public TypeProject create(TypeProjectDto dto) {
        TypeProject typeProject = typeProjectMapper.fromDto(dto, null);
        return typeProjectRepository.save(typeProject);
    }

    @Override
    public TypeProject update(Integer id, TypeProjectDto dto) {
        TypeProject typeProjectFound = typeProjectRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("TypeProject", id));
        TypeProject typeProject = typeProjectMapper.fromDto(dto, typeProjectFound);
        return typeProjectRepository.save(typeProject);
    }

    @Override
    public TypeProject getById(Integer id) {
        TypeProject typeProject = typeProjectRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("typeProject", id));
        return typeProject;
    }

    @Override
    public void delete(Integer id) {
        TypeProject typeProject = typeProjectRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("typeProject", id));
        typeProjectRepository.delete(typeProject);
    }

    @Override
    public TypeProjectPojo getByIdWithProjects(Integer id) {
        TypeProjectPojo typeProjectPojo = new TypeProjectPojo();
        TypeProject typeProject = typeProjectRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("TypeProject", id));
        TypePojo typePojo = typeProjectRepository.findByIdCustom(typeProject.getId());
        List<ProjectPojo> projectsPojo = projectService.findByTypeCustom(typeProject.getId());
        typeProjectPojo.setType(typePojo);
        typeProjectPojo.setProjects(projectsPojo);
        return typeProjectPojo;
    }
}
