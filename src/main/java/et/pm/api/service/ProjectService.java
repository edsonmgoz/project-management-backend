package et.pm.api.service;

import et.pm.api.dto.ProjectDto;
import et.pm.api.entity.Project;

import et.pm.api.pojo.ProjectPojo;
import java.util.List;

public interface ProjectService {

    List<Project> getAll();

    Project create(ProjectDto project);

    Project update(Integer id, ProjectDto project);

    Project getById(Integer id);

    void delete(Integer id);

    List<ProjectPojo> findByTypeCustom(Integer typeProjectId);
}
