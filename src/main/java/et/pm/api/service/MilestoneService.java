package et.pm.api.service;

import et.pm.api.dto.MilestoneDto;
import et.pm.api.entity.Milestone;

import java.util.List;

public interface MilestoneService {

    List<Milestone> getAll();

    Milestone create(MilestoneDto milestone);

    Milestone update(Integer id, MilestoneDto milestone);

    Milestone getById(Integer id);

    void delete(Integer id);
}
