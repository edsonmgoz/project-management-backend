package et.pm.api.service;

import et.pm.api.dto.TaskDto;
import et.pm.api.entity.Task;

import java.util.List;

public interface TaskService {

    List<Task> getAll();

    Task create(TaskDto task);

    Task update(Integer id, TaskDto task);

    Task getById(Integer id);

    void delete(Integer id);
}
