package et.pm.api.service;

import et.pm.api.dto.ProjectDto;
import et.pm.api.entity.Project;
import et.pm.api.entity.TypeProject;
import et.pm.api.exception.EntityNotFoundException;
import et.pm.api.mapper.ProjectMapper;
import et.pm.api.pojo.ProjectPojo;
import et.pm.api.repository.ProjectRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository projectRepository;
    private ProjectMapper projectMapper;
    private TypeProjectService typeProjectService;

    @Override
    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project create(ProjectDto dto) {
        Project project = projectMapper.fromDto(dto, null);
        TypeProject typeProject = typeProjectService.getById(dto.getTypeProjectId());
        project.setTypeProject(typeProject);
        return projectRepository.save(project);
    }

    @Override
    public Project update(Integer id, ProjectDto dto) {
        Project projectFound = projectRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Project", id));
        TypeProject typeProject = typeProjectService.getById(dto.getTypeProjectId());
        Project project = projectMapper.fromDto(dto, projectFound);
        project.setTypeProject(typeProject);
        return projectRepository.save(project);
    }

    @Override
    public Project getById(Integer id) {
        Project project = projectRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Project", id));
        return project;
    }

    @Override
    public void delete(Integer id) {
        Project project = projectRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Project", id));
        projectRepository.delete(project);
    }

    @Override
    public List<ProjectPojo> findByTypeCustom(Integer typeProjectId) {
        TypeProject typeProject = typeProjectService.getById(typeProjectId);
        return projectRepository.findByTypeCustom(typeProject);
    }
}
