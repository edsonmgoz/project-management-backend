package et.pm.api.service;

import et.pm.api.dto.TaskDto;
import et.pm.api.entity.Project;
import et.pm.api.entity.Task;
import et.pm.api.exception.EntityNotFoundException;
import et.pm.api.mapper.TaskMapper;
import et.pm.api.repository.TaskRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private TaskMapper taskMapper;
    private ProjectService projectService;

    @Override
    public List<Task> getAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task create(TaskDto dto) {
        Task task = taskMapper.fromDto(dto, null);
        Project project = projectService.getById(dto.getProjectId());
        task.setProject(project);
        return taskRepository.save(task);
    }

    @Override
    public Task update(Integer id, TaskDto dto) {
        Task taskFound = taskRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Task", id));
        Project project = projectService.getById(dto.getProjectId());
        Task task = taskMapper.fromDto(dto, taskFound);
        task.setProject(project);
        return taskRepository.save(task);
    }

    @Override
    public Task getById(Integer id) {
        Task task = taskRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Task", id));
        return task;
    }

    @Override
    public void delete(Integer id) {
        Task task = taskRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Task", id));
        taskRepository.delete(task);
    }
}
